import Link from 'gatsby-link'
import React, { Component } from 'react'
import IonIcon from 'react-ionicons'
import Aside from '../components/aside'

class Navbar extends Component {
  constructor() {
    super()
    this.state = {
      open: false,
    }
  }
  toggleAside() {
    console.log(this.state.open)

    this.setState({
      open: !this.state.open,
    })
  }
  render() {
    let loginButton
    if (true) {
      loginButton = (
        <Link to="/">
          <IonIcon
            icon="md-contact"
            className="pa2"
            color="white"
            fontSize="30px"
          />
        </Link>
      )
    } else {
      loginButton = (
        <Link className="flex items-center" to="/">
          <IonIcon
            icon="md-log-in"
            className="pa1"
            color="white"
            fontSize="30px"
          />
        </Link>
      )
    }

    let Dimmed = () => {
      if (this.state.open)
        return (
          <div
            className="absolute top-0 bottom-0 left-0 right-0 bg-black o-30 overflow-hidden z-1"
            onClick={() => this.toggleAside()}
          />
        )
      else return null
    }

    return (
      <div>
        <nav className="flex justify-between bg-primary">
          <a href="#">
            <IonIcon
              icon="md-menu"
              className="pa2"
              color="white"
              fontSize="30px"
              onClick={() => this.toggleAside()}
            />
          </a>
          {loginButton}
        </nav>
        <Dimmed />
        <Aside activated={this.state.open} />
      </div>
    )
  }
}

export default Navbar
