import React from 'react'
import Link from 'gatsby-link'
import imageBanner from '../images/banner.jpg'

class Banner extends React.Component {
  render() {
    return (
      <div className="mw-100 fl w-100-l w-100-m w-100-ns pb3 bg-secondary">
        <div className="fl w-100 pa1">
          <h1 className="ma4-l mb0 tc f-headline-sm f-headline-ns  light-color tracked avenir">
            {this.props.title}
          </h1>
        </div>
        <div className="fl w-100 pa1">
          <h2 className="ma1-l tc  f3  medium-color code">
            {this.props.subTitle}
          </h2>
        </div>
      </div>
    )
  }
}
export default Banner
